# 🇧🇷 ALIÁSES
# 🇬🇧 ALIAS

# =========================== #

## 🇧🇷 A utilizar Composer
## 🇬🇧 Using Composer
## 🇪🇸 Usando Composer
## 🇮🇹 Usare Composer
## 🇫🇷 Utiliser Composer
## 🇩🇪 Composer verwenden
## 🇳🇱 Composer gebruiken
## 🇬🇪 Composer-ის გამოყენება
alias composer='composer.phar'

## 🇧🇷 A utilizar o PHP 74
## 🇬🇧 Using PHP 74
## 🇪🇸 Usando PHP 74
## 🇮🇹 Usare PHP 74
## 🇫🇷 Utiliser PHP 74
## 🇩🇪 PHP 74 verwenden
## 🇳🇱 PHP 74 gebruiken
## 🇬🇪 PHP 74-ის გამოყენება
alias php='/opt/cpanel/ea-php74/root/usr/bin/php'

alias glpush="git push --set-upstream git@gitlab.com:gusbemacbe/(git rev-parse --show-toplevel | xargs basename).git (git rev-parse --abbrev-ref HEAD)"
alias sddms="systemctl start sddm"

alias cvbr="corona br"
alias cvge="corona ge"
alias cvit="corona it"
alias cvuk="corona gb"
alias cvus="corona us"

alias code-php="code-insiders --extensions-dir='$HOME/.vscode-insiders/workspaces/php+html+css'"
alias code-markdowm="code-insiders --extensions-dir='$HOME/.vscode-insiders/workspaces/basic markdowm'"
alias code-markdowm-pro="code-insiders --extensions-dir='$HOME/.vscode-insiders/workspaces/html+markdowm'"
alias code-shell="code-insiders --extensions-dir='$HOME/.vscode-insiders/workspaces/basic shell'"
