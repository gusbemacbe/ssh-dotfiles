# lldb
# Autogenerated from man page /usr/share/man/man1/lldb.1.gz
complete -c lldb -l arch --description 'Tells the debugger to use the specified architecture when starting and runnin…'
complete -c lldb -s a --description 'Alias for --arch.'
complete -c lldb -l capture-path --description 'Tells the debugger to use the given filename for the reproducer.'
complete -c lldb -l capture --description 'Tells the debugger to capture a reproducer.'
complete -c lldb -l core --description 'Tells the debugger to use the full path to <filename> as the core file.'
complete -c lldb -s c --description 'Alias for --core.'
complete -c lldb -l debug --description 'Tells the debugger to print out extra information for debugging itself.'
complete -c lldb -s d --description 'Alias for --debug.'
complete -c lldb -l editor --description 'Tells the debugger to open source files using the host\\(aqs "external editor"…'
complete -c lldb -s e --description 'Alias for --editor.'
complete -c lldb -l file --description 'Tells the debugger to use the file <filename> as the program to be debugged.'
complete -c lldb -s f --description 'Alias for --file.'
complete -c lldb -l help --description 'Prints out the usage information for the LLDB debugger.'
complete -c lldb -s h --description 'Alias for --help.'
complete -c lldb -l no-use-colors --description 'Do not use colors.'
complete -c lldb -l replay --description 'Tells the debugger to replay a reproducer from <filename>.'
complete -c lldb -l version --description 'Prints out the current version number of the LLDB debugger.'
complete -c lldb -s v --description 'Alias for --version.'
complete -c lldb -s X --description 'Alias for --no-use-color.'
complete -c lldb -l attach-name --description 'Tells the debugger to attach to a process with the given name.'
complete -c lldb -l attach-pid --description 'Tells the debugger to attach to a process with the given pid.'
complete -c lldb -s n --description 'Alias for --attach-name.'
complete -c lldb -s p --description 'Alias for --attach-pid.'
complete -c lldb -l wait-for --description 'Tells the debugger to wait for a process with the given pid or name to launch…'
complete -c lldb -s w --description 'Alias for --wait-for.'
complete -c lldb -l batch --description 'Tells the debugger to run the commands from -s, -S, -o & -O, and then quit.'
complete -c lldb -s b --description 'Alias for --batch.'
complete -c lldb -s K --description 'Alias for --source-on-crash.'
complete -c lldb -s k --description 'Alias for --one-line-on-crash.'
complete -c lldb -l local-lldbinit --description 'Allow the debugger to parse the .'
complete -c lldb -l no-lldbinit --description 'Do not automatically parse any \'. lldbinit\' files.'
complete -c lldb -l one-line-before-file --description 'Tells the debugger to execute this one-line lldb command before any file prov…'
complete -c lldb -l one-line-on-crash --description 'When in batch mode, tells the debugger to run this one-line lldb command if t…'
complete -c lldb -l one-line --description 'Tells the debugger to execute this one-line lldb command after any file provi…'
complete -c lldb -s O --description 'Alias for --one-line-before-file.'
complete -c lldb -s o --description 'Alias for --one-line.'
complete -c lldb -s Q --description 'Alias for --source-quietly.'
complete -c lldb -l source-before-file --description 'Tells the debugger to read in and execute the lldb commands in the given file…'
complete -c lldb -l source-on-crash --description 'When in batch mode, tells the debugger to source this file of lldb commands i…'
complete -c lldb -l source-quietly --description 'Tells the debugger to execute this one-line lldb command before any file has …'
complete -c lldb -l source --description 'Tells the debugger to read in and execute the lldb commands in the given file…'
complete -c lldb -s S --description 'Alias for --source-before-file.'
complete -c lldb -s s --description 'Alias for --source.'
complete -c lldb -s x --description 'Alias for --no-lldbinit.'
complete -c lldb -s r --description 'Alias for --repl=<flags>.'
complete -c lldb -l repl-language --description 'Chooses the language for the REPL.'
complete -c lldb -l repl --description 'Runs lldb in REPL mode with a stub process with the given flags.'
complete -c lldb -s R --description 'Alias for --repl-language.'
complete -c lldb -s l --description 'Alias for --script-language.'
complete -c lldb -l python-path --description 'Prints out the path to the lldb. py file for this version of lldb.'
complete -c lldb -s P --description 'Alias for --python-path.'
complete -c lldb -l script-language --description 'Tells the debugger to use the specified scripting language for user-defined s…'

