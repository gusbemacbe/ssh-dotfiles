# datetest
# Autogenerated from man page /usr/share/man/man1/datetest.1.gz
complete -c datetest -s h -l help --description 'Print help and exit.'
complete -c datetest -s V -l version --description 'Print version and exit.'
complete -c datetest -s q -l quiet --description 'Suppress message about date/time and duration parser errors.'
complete -c datetest -s i -l input-format --description 'Input format, can be used multiple times.'
complete -c datetest -s b -l base --description 'For underspecified input use DT as a fallback to fill in missing fields.'
complete -c datetest -l from-locale --description 'Interpret dates on stdin or the command line as coming from the locale LOCALE…'
complete -c datetest -l from-zone --description 'Interpret dates on stdin or the command line as coming from the time zone ZON…'
complete -c datetest -s e -l backslash-escapes --description 'Enable interpretation of backslash escapes in the output and input format spe…'
complete -c datetest -l eq --description 'DATE/TIME1 is the same as DATE/TIME2.'
complete -c datetest -l ne --description 'DATE/TIME1 is not the same as DATE/TIME2.'
complete -c datetest -l gt --description 'DATE/TIME1 is newer than DATE/TIME2.'
complete -c datetest -l lt --description 'DATE/TIME1 is older than DATE/TIME2.'
complete -c datetest -l ge --description 'DATE/TIME1 is newer than or equals DATE/TIME2.'
complete -c datetest -l le --description 'DATE/TIME1 is older than or equals DATE/TIME2.'
complete -c datetest -l nt --description 'DATE/TIME1 is newer than DATE/TIME2.'
complete -c datetest -l ot --description 'DATE/TIME1 is older than DATE/TIME2.'
complete -c datetest -l cmp --description 'compare DATE/TIME1 to DATE/TIME2, return with 0 if equal, 1 if left argument …'
complete -c datetest -l isvalid --description 'Return success if dates specified conform to input format.  FORMAT SPECS.'

