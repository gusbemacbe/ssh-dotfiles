# libvirt-dbus
# Autogenerated from man page /usr/share/man/man8/libvirt-dbus.8.gz
complete -c libvirt-dbus -s h -l help --description 'Display command line help usage then exit.'
complete -c libvirt-dbus -l system --description 'Connect to the system bus.'
complete -c libvirt-dbus -l session --description 'Connect to the session bus.'
complete -c libvirt-dbus -s t -l threads --description 'Configure maximal number of worker threads.'

