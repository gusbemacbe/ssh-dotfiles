# php-config80
# Autogenerated from man page /usr/share/man/man1/php-config80.1.gz
complete -c php-config80 -l prefix --description 'Directory prefix where PHP is installed, e. g.  /usr/local.'
complete -c php-config80 -l includes --description 'List of -I options with all include files.'
complete -c php-config80 -l ldflags --description 'LD Flags which PHP was compiled with.'
complete -c php-config80 -l libs --description 'Extra libraries which PHP was compiled with.'
complete -c php-config80 -l man-dir --description 'The directory prefix where the manpages is installed.'
complete -c php-config80 -l extension-dir --description 'Directory where extensions are searched by default.'
complete -c php-config80 -l include-dir --description 'Directory prefix where header files are installed by default.'
complete -c php-config80 -l php-binary --description 'Full path to php CLI or CGI binary.'
complete -c php-config80 -l php-sapis --description 'Show all SAPI modules available.'
complete -c php-config80 -l configure-options --description 'Configure options to recreate configuration of current PHP installation.'
complete -c php-config80 -l version --description 'PHP version.'
complete -c php-config80 -l vernum --description 'PHP version as integer SEE ALSO php (1) VERSION INFORMATION This manpage desc…'

