# rpmdeps
# Autogenerated from man page /usr/share/man/man8/rpmdeps.8.gz
complete -c rpmdeps -s P -l provides --description 'Print the provides.'
complete -c rpmdeps -s R -l requires --description 'Print the requires.'
complete -c rpmdeps -l recommends --description 'Print the recommends.'
complete -c rpmdeps -l suggests --description 'Print the suggests.'
complete -c rpmdeps -l supplements --description 'Print the supplements.'
complete -c rpmdeps -l enhances --description 'Print the enhances.'
complete -c rpmdeps -l conflicts --description 'Print the conflicts.'
complete -c rpmdeps -l obsoletes --description 'Print the obsoletes.'

