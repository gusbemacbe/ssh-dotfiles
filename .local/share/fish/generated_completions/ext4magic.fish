# ext4magic
# Autogenerated from man page /usr/share/man/man8/ext4magic.8.gz
complete -c ext4magic -s M --description 'Try to recover all files.'
complete -c ext4magic -s m --description 'Try to recover only all deleted files.'
complete -c ext4magic -s S --description 'Print the filesystem superblock, the option.'
complete -c ext4magic -s J --description 'Print the content of the Journal superblock.'
complete -c ext4magic -s H --description 'Output a histogram of time stamps from all filesystem Inodes.'
complete -c ext4magic -s V --description 'Print the version of ext4magic and libext2fs.'
complete -c ext4magic -s T --description 'Display the entire transaction list of all copies of data blocks in the Journ…'
complete -c ext4magic -s x --description 'controls optional the output format and the information content of certain co…'
complete -c ext4magic -s B --description 'n  is the data block number of a filesystem datablock.'
complete -c ext4magic -s I --description 'n is the Inode number.'
complete -c ext4magic -s f --description 'the function is the same as   -I n only here is the   <filename> given instea…'
complete -c ext4magic -s s -s n --description 'with this options you can select the backup superblock.'
complete -c ext4magic -s c --description 'This will attempt to find the journal using the data of the superblock.'
complete -c ext4magic -s D --description 'trying a restore of all files from a badly damaged file system.'
complete -c ext4magic -s Q --description 'This is a optional high quality Option for recover and only impact with "  -r…'
complete -c ext4magic -s a --description 'with this option you can set the "  after " time.'
complete -c ext4magic -s b --description 'with this option you can set the "  before " time   n  is the number of secon…'
complete -c ext4magic -s t --description 'is an indirect time option.'
complete -c ext4magic -s j --description 'optional you can select a external copy of the Journal file.'
complete -c ext4magic -s d --description 'select the output directory.  There, the recovered files were written.'
complete -c ext4magic -s i --description 'input_list is a input file.  Must contain a list with double-quoted filenames.'
complete -c ext4magic -s L --description 'Prints the list of all filenames and Inode number of the selected directory t…'
complete -c ext4magic -s l --description 'Prints a list of all filenames which have not allocated data blocks.'
complete -c ext4magic -s r --description 'applied to directories, all files without conflicts with the occupied blocks …'
complete -c ext4magic -s R --description '(show there).'
complete -c ext4magic -o 3day -o 2day --description '.'

