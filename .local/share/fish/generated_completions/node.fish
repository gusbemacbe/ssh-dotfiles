# node
# Autogenerated from man page /usr/share/man/man1/node.1.gz
complete -c node -l secure-heap --description 'Specify the size of the OpenSSL secure heap.'
complete -c node -l secure-heap-min --description 'Specify the minimum allocation from the OpenSSL secure heap.'

