# ocamlrun
# Autogenerated from man page /usr/share/man/man1/ocamlrun.1.gz
complete -c ocamlrun -s b --description 'When the program aborts due to an uncaught exception, print a detailed "back …'
complete -c ocamlrun -s I --description 'Search the directory  dir for dynamically-loaded libraries, in addition to th…'
complete -c ocamlrun -s p --description 'Print the names of the primitives known to this version of  ocamlrun (1) and …'
complete -c ocamlrun -s v --description 'Direct the memory manager to print verbose messages on standard error.'
complete -c ocamlrun -o version --description 'Print version string and exit.'
complete -c ocamlrun -s g --description 'option to ocamlc (1) set.'
complete -c ocamlrun -o vnum --description 'Print short version number and exit.'

