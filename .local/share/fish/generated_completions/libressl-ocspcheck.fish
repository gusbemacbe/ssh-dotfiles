# libressl-ocspcheck
# Autogenerated from man page /usr/share/man/man8/libressl-ocspcheck.8.gz
complete -c libressl-ocspcheck -s C --description 'Specify a PEM format root certificate bundle to use for the validation of req…'
complete -c libressl-ocspcheck -s i --description 'Specify an input filename from which a DER-encoded OCSP response will be read…'
complete -c libressl-ocspcheck -s N --description 'Do not use a nonce value in the OCSP request, or validate that the nonce was …'
complete -c libressl-ocspcheck -s o --description 'Specify an output filename where the DER encoded response from the OCSP serve…'
complete -c libressl-ocspcheck -s v --description 'Increase verbosity.'

