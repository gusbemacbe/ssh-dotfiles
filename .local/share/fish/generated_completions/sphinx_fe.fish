# sphinx_fe
# Autogenerated from man page /usr/share/man/man1/sphinx_fe.1.gz
complete -c sphinx_fe -o alpha --description 'Preemphasis parameter.'
complete -c sphinx_fe -o argfile --description 'file (e. g.  feat. params from an acoustic model) to read parameters from.'
complete -c sphinx_fe -o blocksize --description 'Number of samples to read at a time.'
complete -c sphinx_fe -o build_outdirs --description 'Create missing subdirectories in output directory.'
complete -c sphinx_fe -s c --description 'file for batch processing.'
complete -c sphinx_fe -o cep2spec --description 'Input is cepstral files, output is log spectral files.'
complete -c sphinx_fe -o di --description 'directory, input file names are relative to this, if defined.'
complete -c sphinx_fe -o dither --description 'Add 1/2-bit noise.'
complete -c sphinx_fe -o do --description 'directory, output files are relative to this.'
complete -c sphinx_fe -o doublebw --description 'Use double bandwidth filters (same center freq).'
complete -c sphinx_fe -o ei --description 'extension to be applied to all input files.'
complete -c sphinx_fe -o eo --description 'extension to be applied to all output files.'
complete -c sphinx_fe -o example --description 'Shows example of how to use the tool.'
complete -c sphinx_fe -o frate --description 'Frame rate.'
complete -c sphinx_fe -o help --description 'Shows the usage of the tool.'
complete -c sphinx_fe -s i --description 'audio input file.'
complete -c sphinx_fe -o input_endian --description 'Endianness of input data, big or little, ignored if NIST or MS Wav.'
complete -c sphinx_fe -o lifter --description 'Length of sin-curve for liftering, or 0 for no liftering.'
complete -c sphinx_fe -o logspec --description 'Write out logspectral files instead of cepstra.'
complete -c sphinx_fe -o lowerf --description 'Lower edge of filters.'
complete -c sphinx_fe -o mach_endian --description 'Endianness of machine, big or little.'
complete -c sphinx_fe -o mswav --description 'Defines input format as Microsoft Wav (RIFF).'
complete -c sphinx_fe -o ncep --description 'Number of cep coefficients.'
complete -c sphinx_fe -o nchans --description 'Number of channels of data (interlaced samples assumed).'
complete -c sphinx_fe -o nfft --description 'Size of FFT.'
complete -c sphinx_fe -o nfilt --description 'Number of filter banks.'
complete -c sphinx_fe -o nist --description 'Defines input format as NIST sphere.'
complete -c sphinx_fe -o npart --description 'Number of parts to run in (supersedes -nskip and -runlen if non-zero).'
complete -c sphinx_fe -o nskip --description 'If a control file was specified, the number of utterances to skip at the head…'
complete -c sphinx_fe -s o --description 'cepstral output file.'
complete -c sphinx_fe -o ofmt --description 'Format of output files - one of sphinx, htk, text.'
complete -c sphinx_fe -o part --description 'Index of the part to run (supersedes -nskip and -runlen if non-zero).'
complete -c sphinx_fe -o raw --description 'Defines input format as raw binary data.'
complete -c sphinx_fe -o remove_dc --description 'Remove DC offset from each frame.'
complete -c sphinx_fe -o remove_noise --description 'Remove noise with spectral subtraction in mel-energies.'
complete -c sphinx_fe -o remove_silence --description 'Enables VAD, removes silence frames from processing.'
complete -c sphinx_fe -o round_filters --description 'Round mel filter frequencies to DFT points.'
complete -c sphinx_fe -o runlen --description 'If a control file was specified, the number of utterances to process, or -1 f…'
complete -c sphinx_fe -o samprate --description 'Sampling rate.'
complete -c sphinx_fe -o seed --description 'Seed for random number generator; if less than zero, pick our own.'
complete -c sphinx_fe -o smoothspec --description 'Write out cepstral-smoothed logspectral files.'
complete -c sphinx_fe -o spec2cep --description 'Input is log spectral files, output is cepstral files.'
complete -c sphinx_fe -o sph2pipe --description 'Input is NIST sphere (possibly with Shorten), use sph2pipe to convert.'
complete -c sphinx_fe -o transform --description 'Which type of transform to use to calculate cepstra (legacy, dct, or htk).'
complete -c sphinx_fe -o unit_area --description 'Normalize mel filters to unit area.'
complete -c sphinx_fe -o upperf --description 'Upper edge of filters.'
complete -c sphinx_fe -o vad_postspeech --description 'Num of silence frames to keep after from speech to silence.'
complete -c sphinx_fe -o vad_prespeech --description 'Num of speech frames to keep before silence to speech.'
complete -c sphinx_fe -o vad_startspeech --description 'Num of speech frames to trigger vad from silence to speech.'
complete -c sphinx_fe -o vad_threshold --description 'Threshold for decision between noise and silence frames.'
complete -c sphinx_fe -o verbose --description 'Show input filenames.'
complete -c sphinx_fe -o warp_params --description 'defining the warping function.'
complete -c sphinx_fe -o warp_type --description 'Warping function type (or shape).'
complete -c sphinx_fe -o whichchan --description 'Channel to process (numbered from 1), or 0 to mix all channels.'
complete -c sphinx_fe -o wlen --description 'Hamming window length.'

