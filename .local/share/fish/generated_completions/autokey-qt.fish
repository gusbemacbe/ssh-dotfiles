# autokey-qt
# Autogenerated from man page /usr/share/man/man1/autokey-qt.1.gz
complete -c autokey-qt -l help --description 'Show summary of options.'
complete -c autokey-qt -s l -l verbose --description 'Enable verbose (debug) logging.'
complete -c autokey-qt -s c -l configure --description 'Show the configuration window on startup, even if this is not the first run.'

