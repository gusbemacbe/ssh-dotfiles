# valgrind-di-server
# Autogenerated from man page /usr/share/man/man1/valgrind-di-server.1.gz
complete -c valgrind-di-server -s e -l exit-at-zero --description 'When the number of connected processes falls back to zero, exit.'
complete -c valgrind-di-server -l max-connect --description 'By default, the server can connect to up to 50 processes.'
complete -c valgrind-di-server -l debuginfo-server --description 'option to Valgrind itself.'

