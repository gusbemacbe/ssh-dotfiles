# cargo-init
# Autogenerated from man page /usr/share/man/man1/cargo-init.1.gz
complete -c cargo-init -l bin --description '.'
complete -c cargo-init -l lib --description '.'
complete -c cargo-init -l edition --description '.'
complete -c cargo-init -l name --description '.'
complete -c cargo-init -l vcs --description '.'
complete -c cargo-init -l registry --description '.'
complete -c cargo-init -s v --description '.'
complete -c cargo-init -l verbose --description '.'
complete -c cargo-init -s q --description '.'
complete -c cargo-init -l quiet --description '.'
complete -c cargo-init -l color --description '.'
complete -c cargo-init -s h --description '.'
complete -c cargo-init -l help --description '.'
complete -c cargo-init -s Z --description '.'

