# git-lfs-ls-files
# Autogenerated from man page /usr/share/man/man1/git-lfs-ls-files.1.gz
complete -c git-lfs-ls-files -s l -l long --description 'Show the entire 64 character OID, instead of just first 10.'
complete -c git-lfs-ls-files -s s -l size --description 'Show the size of the LFS object between parenthesis at the end of a line.'
complete -c git-lfs-ls-files -s d -l debug --description 'Show as much information as possible about a LFS file.'
complete -c git-lfs-ls-files -s a -l all --description 'Inspects the full history of the repository, not the current HEAD (or other p…'
complete -c git-lfs-ls-files -l deleted --description 'Shows the full history of the given reference, including objects that have be…'
complete -c git-lfs-ls-files -s I -l include --description 'Include paths matching only these patterns; see [FETCH SETTINGS].'
complete -c git-lfs-ls-files -s X -l exclude --description 'Exclude paths matching any of these patterns; see [FETCH SETTINGS].'

