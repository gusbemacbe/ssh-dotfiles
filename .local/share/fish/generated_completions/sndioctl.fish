# sndioctl
# Autogenerated from man page /usr/share/man/man1/sndioctl.1.gz
complete -c sndioctl -s d --description 'Dump the raw list of available controls and exit.  Useful as a debugging tool.'
complete -c sndioctl -s f --description 'Use this sndio 7 audio device.'
complete -c sndioctl -s i --description 'Display characteristics of requested controls instead of their values.'
complete -c sndioctl -s m --description 'Monitor and display audio controls changes.'
complete -c sndioctl -s n --description 'Suppress printing of the variable name.'
complete -c sndioctl -s q --description 'Suppress all printing when setting a variable.'
complete -c sndioctl -s v --description 'Enable verbose mode, a. k. a.  multi-channel mode.'

