# sphinx_cepview
# Autogenerated from man page /usr/share/man/man1/sphinx_cepview.1.gz
complete -c sphinx_cepview -s b --description 'The beginning frame 0-based.'
complete -c sphinx_cepview -s d --description 'Number of displayed coefficients.'
complete -c sphinx_cepview -o describe --description 'Whether description will be shown.'
complete -c sphinx_cepview -s e --description 'The ending frame.'
complete -c sphinx_cepview -s f --description 'feature file.'
complete -c sphinx_cepview -o header --description 'Whether header is shown.'
complete -c sphinx_cepview -s i --description 'Number of coefficients in the feature vector.'
complete -c sphinx_cepview -o logfn --description 'file (default stdout/stderr) AUTHOR Written by numerous people at CMU from 19…'

