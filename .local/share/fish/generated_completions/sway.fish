# sway
# Autogenerated from man page /usr/share/man/man1/sway.1.gz
complete -c sway -s h -l help --description '.'
complete -c sway -s c -l config --description '.'
complete -c sway -s C -l validate --description '.'
complete -c sway -s d -l debug --description '.'
complete -c sway -s v -l version --description '.'
complete -c sway -s V -l verbose --description '.'
complete -c sway -l get-socketpath --description '.'

