# hugo-gen-chromastyles
# Autogenerated from man page /usr/share/man/man1/hugo-gen-chromastyles.1.gz
complete -c hugo-gen-chromastyles -s h -l help --description '	help for chromastyles.'
complete -c hugo-gen-chromastyles -l highlightStyle --description '	style used for highlighting lines (see https://github.'
complete -c hugo-gen-chromastyles -l linesStyle --description '	style used for line numbers (see https://github. com/alecthomas/chroma).'
complete -c hugo-gen-chromastyles -l style --description '	highlighter style (see https://help. farbox. com/pygments.'
complete -c hugo-gen-chromastyles -l config --description '	config file (default is path/config. yaml|json|toml).'
complete -c hugo-gen-chromastyles -l configDir --description '	config dir.'
complete -c hugo-gen-chromastyles -l debug --description '	debug output.'
complete -c hugo-gen-chromastyles -s e -l environment --description '	build environment.'
complete -c hugo-gen-chromastyles -l ignoreVendor --description '	ignores any _vendor directory.'
complete -c hugo-gen-chromastyles -l ignoreVendorPaths --description '	ignores any _vendor for module paths matching the given Glob pattern.'
complete -c hugo-gen-chromastyles -l log --description '	enable Logging.'
complete -c hugo-gen-chromastyles -l logFile --description '	log File path (if set, logging enabled automatically).'
complete -c hugo-gen-chromastyles -l quiet --description '	build in quiet mode.'
complete -c hugo-gen-chromastyles -s s -l source --description '	filesystem path to read files relative from.'
complete -c hugo-gen-chromastyles -l themesDir --description '	filesystem path to themes directory.'
complete -c hugo-gen-chromastyles -s v -l verbose --description '	verbose output.'
complete -c hugo-gen-chromastyles -l verboseLog --description '	verbose logging SEE ALSO.'

