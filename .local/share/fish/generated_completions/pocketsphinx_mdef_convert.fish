# pocketsphinx_mdef_convert
# Autogenerated from man page /usr/share/man/man1/pocketsphinx_mdef_convert.1.gz
complete -c pocketsphinx_mdef_convert -o text --description 'The input is in text format, and is to be converted to binary.'
complete -c pocketsphinx_mdef_convert -o bin --description 'The input is in binary format, and is to be converted to text.'

