# lprint-status
# Autogenerated from man page /usr/share/man/man1/lprint-status.1.gz
complete -c lprint-status -s d --description 'Specifies the name of a printer.'
complete -c lprint-status -s u --description 'Specifies an "ipp:" or "ipps:" URI for a remote printer.'

