# ms_print
# Autogenerated from man page /usr/share/man/man1/ms_print.1.gz
complete -c ms_print -s h -l help --description 'Show the help message.'
complete -c ms_print -l version --description 'Show the version number.'
complete -c ms_print -l threshold --description 'Same as Massif\\*(Aqs --threshold option, but applied after profiling rather t…'
complete -c ms_print -l x --description 'Width of the graph, in columns.'
complete -c ms_print -l y --description 'Height of the graph, in rows.'

