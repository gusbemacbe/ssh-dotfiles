# cargo-search
# Autogenerated from man page /usr/share/man/man1/cargo-search.1.gz
complete -c cargo-search -l limit --description '.'
complete -c cargo-search -l index --description '.'
complete -c cargo-search -l registry --description '.'
complete -c cargo-search -s v --description '.'
complete -c cargo-search -l verbose --description '.'
complete -c cargo-search -s q --description '.'
complete -c cargo-search -l quiet --description '.'
complete -c cargo-search -l color --description '.'
complete -c cargo-search -s h --description '.'
complete -c cargo-search -l help --description '.'
complete -c cargo-search -s Z --description '.'

