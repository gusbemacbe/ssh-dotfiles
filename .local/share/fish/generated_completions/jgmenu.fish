# jgmenu
# Autogenerated from man page /usr/share/man/man1/jgmenu.1.gz
complete -c jgmenu -l no-spawn --description 'Redirect command to stdout rather than execute it.'
complete -c jgmenu -l checkout --description 'Checkout submenu on startup.'
complete -c jgmenu -l config-file --description 'Read config file.'
complete -c jgmenu -l icon-size --description 'Specify icon size (22 by default).  If set to 0, icons will not be loaded.'
complete -c jgmenu -l at-pointer --description 'Launch menu at mouse pointer.'
complete -c jgmenu -l hide-on-startup --description 'Start menu is hidden state.'
complete -c jgmenu -l simple --description 'Ignore tint2 settings; Run in short-lived mode (i. e.'
complete -c jgmenu -l vsimple --description 'Same as --simple, but also disables icons and ignores jgmenurc.'
complete -c jgmenu -l csv-file --description 'Specify menu file (in jgmenu flavoured CSV format).'
complete -c jgmenu -l csv-cmd --description 'Specify command to produce menu data, for example jgmenu_run pmenu.'
complete -c jgmenu -l die-when-loaded --description 'Open menu and then exit(0).  Useful for debugging and testing.'
complete -c jgmenu -l center --description 'Center align menu horizontally and vertically.'
complete -c jgmenu -l persistent --description 'Same as the persistent config option.'

