# php-fpm80
# Autogenerated from man page /usr/share/man/man8/php-fpm80.8.gz
complete -c php-fpm80 -s C --description 'Do not chdir to the script\'s directory.'
complete -c php-fpm80 -s c --description 'Look for  php. ini file in the directory R path or use the specified R file.'
complete -c php-fpm80 -s n --description 'No  php. ini file will be used.'
complete -c php-fpm80 -s d --description 'Define INI entry R foo with value R bar.'
complete -c php-fpm80 -s e --description 'Generate extended information for debugger/profiler.'
complete -c php-fpm80 -s h --description 'This help.'
complete -c php-fpm80 -s i --description 'PHP information and configuration.'
complete -c php-fpm80 -s m --description 'Show compiled in modules.'
complete -c php-fpm80 -s v --description 'Version number  --prefix path.'
complete -c php-fpm80 -s p --description 'Specify alternative prefix path (the default is /usr).'
complete -c php-fpm80 -s g --description 'Specify the PID file location.'
complete -c php-fpm80 -s y --description 'Specify alternative path to FastCGI process manager configuration file (the d…'
complete -c php-fpm80 -s t --description 'Test FPM configuration file and exit If called twice (-tt), the configuration…'
complete -c php-fpm80 -s D --description 'Force to run in background and ignore daemonize option from configuration fil…'
complete -c php-fpm80 -s F --description 'Force to stay in foreground and ignore daemonize option from configuration fi…'
complete -c php-fpm80 -s O --description 'Force output to stderr in nodaemonize even if stderr is not a TTY.'
complete -c php-fpm80 -l php-ini --description '.'
complete -c php-fpm80 -l no-php-ini --description '.'
complete -c php-fpm80 -l define --description '.'
complete -c php-fpm80 -l help --description '.'
complete -c php-fpm80 -l info --description '.'
complete -c php-fpm80 -l modules --description '.'
complete -c php-fpm80 -l version --description '.'
complete -c php-fpm80 -l prefix --description '.'
complete -c php-fpm80 -l pid --description '.'
complete -c php-fpm80 -l fpm-config --description '.'
complete -c php-fpm80 -l test --description '.'
complete -c php-fpm80 -l daemonize --description '.'
complete -c php-fpm80 -l nodaemonize --description '.'
complete -c php-fpm80 -l force-stderr --description '.'
complete -c php-fpm80 -l allow-to-run-as-root --description '.'
complete -c php-fpm80 -s R --description 'Allow pool to run as root (disabled by default) FILES.'

