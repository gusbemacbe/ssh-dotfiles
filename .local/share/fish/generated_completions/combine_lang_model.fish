# combine_lang_model
# Autogenerated from man page /usr/share/man/man1/combine_lang_model.1.gz
complete -c combine_lang_model -l lang --description 'The language to use.  Tesseract uses 3-character ISO 639-2 language codes.'
complete -c combine_lang_model -l script_dir --description 'Directory name for input script unicharsets.'
complete -c combine_lang_model -l input_unicharset --description 'Unicharset to complete and use in encoding.'
complete -c combine_lang_model -l lang_is_rtl --description 'True if language being processed is written right-to-left (eg Arabic/Hebrew).'
complete -c combine_lang_model -l pass_through_recoder --description 'If true, the recoder is a simple pass-through of the unicharset.'
complete -c combine_lang_model -l version_str --description 'An arbitrary version label to add to traineddata file (type:string default:).'
complete -c combine_lang_model -l words --description '(Optional) File listing words to use for the system dictionary (type:string d…'
complete -c combine_lang_model -l numbers --description '(Optional) File listing number patterns (type:string default:).'
complete -c combine_lang_model -l puncs --description '(Optional) File listing punctuation patterns.'
complete -c combine_lang_model -l output_dir --description 'Root directory for output files.'

