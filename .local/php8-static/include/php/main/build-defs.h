/*
   +----------------------------------------------------------------------+
   | Copyright (c) The PHP Group                                          |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Author: Stig Sæther Bakken <ssb@php.net>                             |
   +----------------------------------------------------------------------+
*/

#define CONFIGURE_COMMAND " './configure'  '--prefix=/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static' '--with-config-file-path=/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/php' '--with-config-file-scan-dir=/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/php/conf.d'"
#define PHP_ODBC_CFLAGS	""
#define PHP_ODBC_LFLAGS		""
#define PHP_ODBC_LIBS		""
#define PHP_ODBC_TYPE		""
#define PHP_OCI8_DIR			""
#define PHP_OCI8_ORACLE_VERSION		""
#define PHP_PROG_SENDMAIL	"/usr/bin/sendmail"
#define PEAR_INSTALLDIR         ""
#define PHP_INCLUDE_PATH	".:"
#define PHP_EXTENSION_DIR       "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/lib/php/extensions/no-debug-non-zts-20201009"
#define PHP_PREFIX              "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static"
#define PHP_BINDIR              "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/bin"
#define PHP_SBINDIR             "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/sbin"
#define PHP_MANDIR              "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/php/man"
#define PHP_LIBDIR              "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/lib/php"
#define PHP_DATADIR             "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/share/php"
#define PHP_SYSCONFDIR          "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/etc"
#define PHP_LOCALSTATEDIR       "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/var"
#define PHP_CONFIG_FILE_PATH    "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/php"
#define PHP_CONFIG_FILE_SCAN_DIR    "/home/gusbemacbe/Transferências/Linux/Source/php-src-master/php8-static/php/conf.d"
#define PHP_SHLIB_SUFFIX        "so"
#define PHP_SHLIB_EXT_PREFIX    ""
