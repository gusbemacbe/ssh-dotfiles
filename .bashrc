# 🇧🇷 Caminho para sua instalação de OhMyBash.
# 🇬🇧 Path to your OhMyBash installation.
# 🇪🇸 Ruta a tu instalación de OhMyBash.
# 🇪🇸 Camí de la teva instal·lació de OhMyBash.
# 🇮🇹 Percorso per la tua installazione di OhMyBash.
# 🇫🇷 Chemin vers votre installation de OhMyBash.
# 🇩🇪 Pfad zu deine OhMyBash-Installation.
# 🇳🇱 Pad naar uw OhMyBash-installatie.
# 🇷🇴 Calea către instalarea ta OhMyBash.
# 🇬🇷 Διαδρομή στην εγκατάσταση του OhMyBash.
# 🇬🇪 თქვენი OhMyBash-ის ინსტალაციისკენ მიმავალი გზა.
export OSH=$HOME/.oh-my-bash

# 🇧🇷 Defina o nome do tema a ser carregado. Opcionalmente, se você definir isso como "aleatório", ele carregará um tema aleatório cada vez que o oh-my-bash for carregado.
# 🇬🇧 Set name of the theme to load. Optionally, if you set this to "random", it'll load a random theme each time that oh-my-bash is loaded.
OSH_THEME="agnoster"

# 🇧🇷 Usar o preenchimento com distinção entre maiúsculas e minúsculas.
# 🇬🇧 To use case-sensitive completion.
CASE_SENSITIVE="true"

# 🇧🇷 Usar o preenchimento insensível a hífen. A conclusão com distinção entre maiúsculas e minúsculas deve estar desativada._ e - serão intercambiáveis.
# 🇬🇧 Use hyphen-insensitive completion. Case sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# 🇧🇷 Desative as verificações de atualização automática quinzenais.
# 🇬🇧 Disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# 🇧🇷 Habilitar a correção automática do comando.
# 🇬🇧 Enable command auto-correction.
ENABLE_CORRECTION="true"

# 🇧🇷 Exibir pontos vermelhos enquanto espera pela conclusão.
# 🇬🇧 Display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# 🇧🇷 Se você deseja alterar o carimbo de hora de execução do comando mostrado na saída do comando de histórico.
# 🇬🇧 If you want to change the command execution time stamp shown in the history command output.

# 🇧🇷 Os três formatos opcionais: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# 🇬🇧 The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd/mm/yyyy"

completions=(
  composer
  git
  ssh
)

aliases=(
  general
)

plugins=(
  bashmarks
  git
)

source $OSH/oh-my-bash.sh
source ~/.local/bin/bashmarks.sh

# 🇧🇷 Definições globais de origem
# 🇬🇧 Source global definitions
# 🇪🇸 Definiciones globales de origen
# 🇮🇹 Definizioni globali di origine
# 🇫🇷 Définitions globales de la source
# 🇩🇪 Globale Quelldefinitionen
# 🇳🇱 Globale brondefinities
# 🇬🇪 გლობალური წყარო განმარტებები
# if [ -f /etc/bashrc ]; then
# 	. /etc/bashrc
# fi

# 🇧🇷 Usar o modo de microedição por padrão
# 🇬🇧 Use micro-editing mode by default
# 🇪🇸 Usar el modo de microedición por defecto
# 🇮🇹 Usare la modalità di micro-edizione per impostazione predefinita
# 🇫🇷 Utiliser le mode de micro-édition par défaut
# 🇩🇪 Standardmäßig den Mikrobearbeitungsmodus verwenden
# 🇳🇱 Standaard de microbewerkingsmodus gebruiken
# 🇬🇪 ნაგულისხმევად გამოყენებით მიკრო-რედაქტორი რეჟიმი
export EDITOR="~/.local/bin/micro"

# 🇧🇷 Aliases e funções específicos do utilizador
# 🇬🇧 User specific aliases and functions
# 🇪🇸 Alias y funciones específicos del usuario
# 🇮🇹 Alias e funzioni specifici dell'utente
# 🇫🇷 Alias et fonctions spécifiques à l'utilisateur
# 🇩🇪 Benutzerspezifische Aliase und Funktionen
# 🇳🇱 Gebruikersspecifieke aliassen en functies
# 🇬🇪 მომხმარებლის სპეციფიკური alias და ფუნქციები

## Git 2.30
#export PATH="/home4/lermam39/.local/git-static/bin:$PATH"

## 🇧🇷 Pasta ~/.local/bin
## 🇬🇧 Folder ~/.local/bin
## 🇪🇸 Carpeta ~/.local/bin
## 🇮🇹 Cartella ~/.local/bin
## 🇫🇷 Dossier ~/.local/bin
## 🇩🇪 Ordner ~/.local/bin
## 🇳🇱 Map ~/.local/bin
## 🇬🇪 საქაღალდე ~/.local/bin
export PATH=~/.local/bin:$PATH
export PATH=~/.local/fish/bin:$PATH
export PATH=~/.local/zsh-bin/bin:$PATH

## 🇧🇷 A utilizar o PHP 74
## 🇬🇧 Using PHP 74
## 🇪🇸 Usando PHP 74
## 🇮🇹 Usare PHP 74
## 🇫🇷 Utiliser PHP 74
## 🇩🇪 PHP 74 verwenden
## 🇳🇱 PHP 74 gebruiken
## 🇬🇪 PHP 74-ის გამოყენება
alias php='/opt/cpanel/ea-php74/root/usr/bin/php'
export PATH="/opt/cpanel/ea-php74/root/usr/bin:$PATH"

## 🇧🇷 A utilizar Composer
## 🇬🇧 Using Composer
## 🇪🇸 Usando Composer
## 🇮🇹 Usare Composer
## 🇫🇷 Utiliser Composer
## 🇩🇪 Composer verwenden
## 🇳🇱 Composer gebruiken
## 🇬🇪 Composer-ის გამოყენება
alias composer='composer.phar'

# 🇧🇷 Utilizar Fish como novo padrão
# 🇬🇧 Use Fish as a new default shell
# 🇪🇸 Utilizar Fish como un nuevo caparazón predeterminado
# 🇮🇹 Usare Fish come una nuova shell predefinita
# 🇫🇷 Utiliser Fish comme nouveau shell par défaut
# 🇩🇪 Fish verwenden als neue Standard-Shell
# 🇳🇱 Fish gebruiken als een nieuwe standaard shell
# 🇬🇪 Fish-ის, როგორც ახალი ნაგულისხმევი shell-ის გამოყენება
# ~/.local/fish/bin/fish